# Ishihara Generator #

![lenny.png](https://bitbucket.org/repo/GBBBno/images/1175412321-lenny.png)

[Ishihara Generator](http://cptblack.cf/ishihara) is a online tool to generate fake red-green colourblindness test plates. 

Enter an image URL or choose a preset to generate a plate!