#!/home/cptblack/python/Python-2.7.2/python

import cgitb
cgitb.enable()

import sys

packagePath = '/home/cptblack/.local/lib/python2.7/site-packages/'
sys.path.append(packagePath)


from PIL import Image,ImageDraw
import cStringIO
import cgi
from random import randint as rint



X,Y = 500, 275 #image width and height

def randGradient():
    img = Image.new("RGB", (300,300), "#FFFFFF")
    draw = ImageDraw.Draw(img)

    r,g,b = rint(0,255), rint(0,255), rint(0,255)
    dr = (rint(0,255) - r)/300.
    dg = (rint(0,255) - g)/300.
    db = (rint(0,255) - b)/300.
    for i in range(300):
        r,g,b = r+dr, g+dg, b+db
        draw.line((i,0,i,300), fill=(int(r),int(g),int(b)))

    #write to file object
    f = cStringIO.StringIO()
    img.save(f, "PNG")
    f.seek(0)

    #output to browser
    print "Content-type: image/png\n"
    print f.read()

if __name__ == "__main__":
    form = cgi.FieldStorage()
    randGradient()
