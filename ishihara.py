#!/home/cptblack/python/Python-2.7.2/python

import cgitb
cgitb.enable()

import sys
packagePath = '/home/cptblack/.local/lib/python2.7/site-packages/'
sys.path.append(packagePath)

from PIL import Image,ImageDraw,ImageOps
import cStringIO
import cgi

import colorsys, random, math, struct, urllib2

width, height = 1, 1
boundingCircle = None
SPRITE_404 = '404.jpg'
SPRITE_NOT_URL = 'not a url.jpg'
SPRITE_DEFAULT = 'http://vignette1.wikia.nocookie.net/thelennyface/images/2/24/Lenny_face.png/revision/latest?cb=20140728000856';
MAX_WIDTH = 640;
MAX_HEIGHT = 480;

################### Objects and helper functions

def hexToRgb(hexVal):
    # Remove the '#' if there is one.
    if hexVal[0] == "#":
        hexVal = hexVal[1:]
    return struct.unpack('BBB', hexVal.decode('hex'))

def rgbToHex(rgbVal):
    return "#" + struct.pack('BBB', *rgbVal).encode('hex')

class Circle:
    maxSize = 35
    minSize = 15
    drawnCircles = []
    numBuckets = (1, 1)
    
    BG_COLORS = ["#ee7326", # Orange
                 "#ffc86f", # Light orange
                 "#f6c872", # Light orange #2
                 "#f6e6a8", # Lightest orange
                 "#f9a374", # Peach
                 #"#c07f65", # Light crimson
                 #"#e79461", # Another orange
                 #"#a56449", # Rougey
                 ]
    FG_COLORS = ["#84b13c", # Green
                 "#99b636", # Light green
                 "#d7e1a2", # Pale blue-green
                 "#589456", # Blue-green
                 #"#36a96c", # Sea green
                 #"#8ab472", # Mint
                 ]
    MG_COLORS = ["#b1aa3c",
                 "#b6a236",
                 "#e1c9a2",
                 "#899456",
                 ]

    # Let's make the FG colours a little darker and more saturated.
    for i, color in enumerate(FG_COLORS):
        r, g, b = hexToRgb(color)
        hue, lum, sat = colorsys.rgb_to_hls(r, g, b)
        lum *= 0.8
        sat *= 1.2
        r, g, b = colorsys.hls_to_rgb(hue, lum, sat)
        r, g, b = min(r, 255), min(g, 255), min(b, 255)
        FG_COLORS[i] = rgbToHex((r,g,b))
        
    
    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.r = radius

    def getBucket(self):
        bucketSize = Circle.maxSize
        return (
            min(int(math.floor(self.y / bucketSize)), Circle.numBuckets[0] - 1),
            min(int(math.floor(self.x / bucketSize)), Circle.numBuckets[1] - 1)
        )
        
    def intersectsCircle(self, other):
        centreDist = math.sqrt((self.x - other.x)**2 + (self.y - other.y)**2)
        return self.r + other.r >= centreDist

    @classmethod
    def isValidBucket(cls, bucket):
        if bucket[0] >= 0 and bucket[1] >= 0 and \
            bucket[0] < cls.numBuckets[0] and bucket[1] < cls.numBuckets[1]:
            return True
        return False

    @classmethod
    def getRelevantBuckets(cls, bucket):
        relevant = []
        bucketY = bucket[0]
        bucketX = bucket[1]
        relevantDeltas = [(-1, -1), (-1, 0), (-1, 1),
                          (0, -1), (0, 0), (0, 1),
                          (1, -1), (1, 0), (1, 1)]
        for deltaY, deltaX in relevantDeltas:
            b = (bucketY + deltaY, bucketX + deltaX)
            if cls.isValidBucket(b):
                relevant.append(b)
        return relevant

    def intersectsAnyCircle(self):
        # To save time, only consider nearby circles.
        bucket = self.getBucket()
        relevantBuckets = Circle.getRelevantBuckets(bucket)
        for buck in relevantBuckets:
            y, x = buck
            buckCircles = Circle.drawnCircles[y][x]
            for other in buckCircles:
                if self.intersectsCircle(other):
                    return True
        return False
    
    def origin(self):
        return (self.x - self.r, self.y - self.r)

    def draw(self, drawingBoard, colorSet="BG"):
        colors = Circle.BG_COLORS
        if colorSet == "FG":
            colors = Circle.FG_COLORS
        if colorSet == "MG":
            colors = Circle.MG_COLORS
        left, top = self.origin()
        drawingBoard.ellipse((left, top, left+self.r*2, top+self.r*2), fill=random.choice(colors))

        # Which bucket does the circle belong to?
        buck = self.getBucket()
        Circle.drawnCircles[buck[0]][buck[1]].append(self)
    
    def moveToRandomPos(self):
        self.x, self.y = random.randint(0, width - 1), random.randint(0, height - 1)
        
    def setRandomSize(self):
        self.r = random.randint(Circle.minSize, Circle.maxSize)

    def shrinkSize(self):
        self.r *= 0.8

    def growSize(self):
        self.r *= 1.1

    def isValidSize(self):
        return self.r >= Circle.minSize
        
    def validInDimensions(self):
        xValid = self.x - self.r >= 0 and self.x + self.r <= width
        yValid = self.y - self.r >= 0 and self.y + self.r <= height
        return xValid and yValid

    def fitsInBoundingCircle(self):
        return self.intersectsCircle(boundingCircle)
    
    def __str__(self):
        return "Circle (%d, %d, %d)" % (self.x, self.y, self.r)
    
    def __repr__(self):
        return str(self)
    
################## Main program

def ishihara(filename):
    global width
    global height
    global boundingCircle
    
    # Load the sprite image
    should_ishiharize = True
    try:
        f = cStringIO.StringIO(urllib2.urlopen(filename).read())
        sprite = Image.open(f)
    except urllib2.URLError:
        # Invalid URL. Complain.
        im = Image.open(SPRITE_404)
        should_ishiharize = False
    except ValueError:
        # Not a URL at all. Complain.
        im = Image.open(SPRITE_NOT_URL)
        should_ishiharize = False
    
    if should_ishiharize:
        # Convert the sprite to greyscale.
        sprite = sprite.convert("L")

        # Do a quick analysis of the black/white levels.
        pixels = sprite.getdata()
        averagePixel = sum(pixels) / len(pixels)

        width, height = sprite.size
        # I can't even with huge images.
        if width > MAX_WIDTH:
            ratio = MAX_WIDTH/float(width);
            width, height = MAX_WIDTH, int(height*ratio)
            sprite = sprite.resize((width, height))
        if height > MAX_HEIGHT:
            ratio = MAX_HEIGHT/float(height);
            width, height = int(width*ratio), MAX_HEIGHT
            sprite = sprite.resize((width, height))

        # Double the sprite and ishihara size for antialiasing later
        width, height = width*2, height*2
        sprite = sprite.resize((width, height))
        im = Image.new("RGB", sprite.size, "#FFFFFF")
        draw = ImageDraw.Draw(im)

        # Set up the bounding circle
        shortestSide = min(width, height)
        longestSide = max(width, height)
        Circle.minSize = shortestSide/100
        Circle.maxSize = Circle.minSize * 3
        boundingCircle = Circle(width*0.5, height*0.5, longestSide/2)

        # Set up buckets
        # To avoid taking too long looking up circle intersections, we split the
        # image into "buckets" (grid cells) and only bother checking for
        # intersections within the closest buckets.
        numHorizBuckets = width / Circle.maxSize
        numVertBuckets = height / Circle.maxSize
        Circle.numBuckets = (numVertBuckets, numHorizBuckets)
        for i in range(numVertBuckets):
            Circle.drawnCircles.append([])
            for j in range(numHorizBuckets):
                Circle.drawnCircles[i].append([])

        # Draw dots
        numCircles = numHorizBuckets * numVertBuckets
        for i in range(numCircles):
            c = Circle(0, 0, 0)
            # Select a random position and radius.
            c.moveToRandomPos()
            c.setRandomSize()

            repositionCounter = 0
            while (c.intersectsAnyCircle() or not c.fitsInBoundingCircle()):
                c.moveToRandomPos()
                repositionCounter += 1

                # Shrink the circle after every 10 repositionings
                if repositionCounter % 10 == 0: #and c.isValidSize()
                    c.shrinkSize()
                # Give up after a few hundred tries and just stick it anywhere
                if repositionCounter > 100:
                    break
                
            # Now to choose the circle colour!
            pixel = sprite.getpixel((c.x, c.y))
            if pixel < averagePixel:
                # Dark pixel? Use FG color.
                c.growSize()
                c.draw(draw, "FG")
            else:
                # Otherwise, use a BG color.
                c.shrinkSize()
                c.draw(draw, "BG") 

        # Restore to original size
        im.thumbnail((width/2, height/2), Image.ANTIALIAS)
    
    # Write to file object
    f = cStringIO.StringIO()
    im.save(f, "PNG")
    f.seek(0)

    # Output to browser
    print "Content-type: image/png\n"
    print f.read()

if __name__ == "__main__":
    form = cgi.FieldStorage()
    if "url" in form:
        ishihara(form["url"].value)
    else:
        ishihara(SPRITE_DEFAULT)
