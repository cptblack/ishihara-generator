function updateIshihara() {
	// Switch out for a loading gif
	$("#ishihara").hide();
	$("#loading").show();
	url = $("#image-url").val();
	$("#ishihara").attr("src", "ishihara.py?url="+url);
	$("#ishihara").load(function() {
		// Restore the ishihara, now that it's loaded
		$("#loading").hide();
		$("#ishihara").show();
	});
}

function hideJsWarning() {
	// Remove the JS warning.
	$("#js-warning").remove();
}

function onLoad() {
	// Make the form visible.
	$("#form").show();

	// Add form watermarks.
	$("#image-url").Watermark("Image URL");
	// Clicking on the textbox should select the text inside, for easy changing.
	$("#image-url").focus(function() { $(this).select(); });

	// Clicking the thumbnails should select them as the source.
	$(".thumbnail").each( function(i, thumb) {
		$(thumb).click({url: $(thumb).attr("src")}, function(event) {
			$("#image-url").val(event.data.url);
			updateIshihara();
		});
	});

	// Submitting the form should change show the image and change
	// its source URL.
	$("button").click(updateIshihara);

	// Add the Facebook share/like button.
	fbLike = $('<div></div>').attr('class', 'fb-like')
			.attr('data-href', 'http://www.cptblack.cf/ishihara/')
			.attr('data-layout', 'button_count')
			.attr('data-action', 'like')
			.attr('data-show-faces', 'false')
			.attr('data-share', 'true');
	$("#fb-container").append(fbLike);
}